# README #

this module enabled svg and pdf files to be uploaded from the Magento backend.
The thumbnail does not appear in the backend; however it can be selected.
The cms wysiwyg directive for the svg may need to be adjusted 

For instance, it may produce the format like below and the string &quote; needs to be replaced by " instead
<img src="{{media url=&quot;wysiwyg/TS-997242-25-10-2020_1.pdf&quot;}}" alt="" />

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/SavAndPdf when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

If the feature is not working, it may be an idea to flush the browser cache as this feature is heavily reliant on javascript
